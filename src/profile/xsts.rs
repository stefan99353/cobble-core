use crate::{consts, error::AuthResult};
use serde::Deserialize;
use serde_json::json;

#[derive(Debug, Clone)]
pub struct XstsToken {
    pub access_token: String,
}

#[instrument(name = "authenticate_xsts", level = "trace", skip_all)]
pub async fn authenticate(xbl_token: &str) -> AuthResult<XstsToken> {
    let client = reqwest::Client::new();
    let response = client
        .post(consts::XSTS_TOKEN_URL)
        .json(&json!({
            "Properties": {
                "SandboxId": "RETAIL",
                "UserTokens": [
                    xbl_token
                ]
            },
            "RelyingParty": "rp://api.minecraftservices.com/",
            "TokenType": "JWT"
        }))
        .send()
        .await?
        .error_for_status()?
        .json::<XboxLiveSecurityTokenResponse>()
        .await?;

    Ok(XstsToken {
        access_token: response.token,
    })
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct XboxLiveSecurityTokenResponse {
    token: String,
}
