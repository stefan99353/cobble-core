use std::path::PathBuf;

use crate::error::CobbleResult;
use crate::minecraft::{load_servers, Server};
use crate::Instance;

impl Instance {
    /// Path to the .minecraft/servers.dat file.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "servers")))]
    pub fn servers_file(&self) -> PathBuf {
        let mut servers_file = self.dot_minecraft_path();
        servers_file.push("servers.dat");
        servers_file
    }

    /// Loads all screenshots from this instance.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "servers")))]
    pub async fn load_servers(&self) -> CobbleResult<Vec<Server>> {
        load_servers(self.servers_file()).await
    }

    /// Adds a new server to this instance.
    /// Also sets the correct path to this instances servers.dat file.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "servers")))]
    pub async fn add_server(&self, mut server: Server) -> CobbleResult<()> {
        server.path = self.servers_file();
        Server::add(server).await
    }
}
