use crate::error::CobbleResult;
use crate::minecraft::{load_log_files, LogFile};
use crate::Instance;

impl Instance {
    /// Loads all log files from this instance.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "log-files")))]
    pub async fn load_log_files(&self) -> CobbleResult<Vec<LogFile>> {
        load_log_files(self.dot_minecraft_path()).await
    }
}
