use crate::instance::Instance;
use std::path::PathBuf;

impl Instance {
    /// Path to the instance folder.
    pub fn instance_path(&self) -> PathBuf {
        self.instance_path.clone()
    }

    /// Path to the .minecraft folder.
    pub fn dot_minecraft_path(&self) -> PathBuf {
        let mut path = self.instance_path();
        path.push(".minecraft");
        path
    }

    /// Path to the libraries folder.
    pub fn libraries_path(&self) -> PathBuf {
        self.libraries_path.clone()
    }

    /// Path to the assets folder.
    pub fn assets_path(&self) -> PathBuf {
        self.assets_path.clone()
    }

    /// Path to the natives folder.
    pub fn natives_path(&self) -> PathBuf {
        let mut path = self.instance_path();
        path.push("natives");
        path
    }

    /// Path to the resources folder.
    pub fn resources_path(&self) -> PathBuf {
        let mut path = self.assets_path();
        path.push("resources");
        path
    }

    /// Path to the asset indexes folder.
    pub fn asset_indexes_path(&self) -> PathBuf {
        let mut path = self.assets_path();
        path.push("indexes");
        path
    }

    /// Path to the log configs folder.
    pub fn log_configs_path(&self) -> PathBuf {
        let mut path = self.assets_path();
        path.push("log_configs");
        path
    }

    /// Path to the version data JSON file.
    pub fn version_data_path(&self) -> PathBuf {
        let mut path = self.instance_path();
        path.push("version.json");
        path
    }

    /// Path to the version data JSON file.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
    #[cfg(feature = "fabric")]
    pub fn fabric_version_data_path(&self) -> PathBuf {
        let mut path = self.instance_path();
        path.push("fabric_version.json");
        path
    }
}
