use crate::error::CobbleResult;
use crate::minecraft::{load_shaderpacks, parse_shaderpack, Shaderpack};
use crate::Instance;
use std::path::{Path, PathBuf};
use tokio::fs::create_dir_all;

impl Instance {
    /// Path to the .minecraft/resourcepacks or .minecraft/texturepacks folder.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "shaderpacks")))]
    pub fn shaderpacks_path(&self) -> PathBuf {
        let mut shaderpacks_path = self.dot_minecraft_path();

        shaderpacks_path.push("shaderpacks");

        shaderpacks_path
    }

    /// Loads all shaderpacks from this instance.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "shaderpacks")))]
    pub async fn load_shaderpacks(&self) -> CobbleResult<Vec<Shaderpack>> {
        load_shaderpacks(self.dot_minecraft_path()).await
    }

    /// Adds a shaderpack.
    ///
    /// Returns `None` when `src` is not a valid shaderpack.
    #[instrument(name = "add_shaderpack", level = "trace", skip_all, fields(src))]
    #[cfg_attr(doc_cfg, doc(cfg(feature = "shaderpacks")))]
    pub async fn add_shaderpack(&self, src: impl AsRef<Path>) -> CobbleResult<Option<Shaderpack>> {
        trace!("Validating src...");
        if parse_shaderpack(PathBuf::from(src.as_ref()))
            .await?
            .is_none()
        {
            return Ok(None);
        }

        trace!("Building new path for shaderpack");
        let file_name = src
            .as_ref()
            .file_name()
            .ok_or_else(|| std::io::Error::new(std::io::ErrorKind::Other, "Path ends with '..'"))?;
        let mut shaderpack_path = self.shaderpacks_path();
        shaderpack_path.push(file_name);
        tracing::Span::current().record("dest", shaderpack_path.to_string_lossy().to_string());

        if let Some(parent) = shaderpack_path.parent() {
            trace!("Creating shaderpacks folder...");
            create_dir_all(parent).await?;
        }

        trace!("Copying shaderpack...");
        tokio::fs::copy(src, &shaderpack_path).await?;

        trace!("Parsing new shaderpack...");
        let shaderpack = parse_shaderpack(shaderpack_path).await?;
        Ok(shaderpack)
    }
}
