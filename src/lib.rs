//! # cobble-core
//!
//! [![crates.io](https://img.shields.io/crates/v/cobble-core.svg)](https://crates.io/crates/cobble-core)
//! [![Documentation](https://docs.rs/cobble-core/badge.svg)](https://docs.rs/cobble-core)
//! [![MSRV](https://img.shields.io/badge/MSRV-1.65-lightgray.svg)](https://blog.rust-lang.org/2022/11/03/Rust-1.65.0.html)
//! [![Dependency Status](https://deps.rs/repo/gitlab/stefan99353/cobble-core/status.svg)](https://deps.rs/repo/gitlab/stefan99353/cobble-core)
//! [![License](https://img.shields.io/crates/l/cobble-core)](https://opensource.org/licenses/MIT)
//! [![Pipeline](https://gitlab.com/stefan99353/cobble-core/badges/main/pipeline.svg)](https://gitlab.com/stefan99353/cobble-core/-/pipelines)
//!
//! **A Rust library for managing, installing and launching Minecraft instances and more.**
//!
//! This crate provides the following features (some are locked behind [features](#crate-features)):
//!
//! - Complete installation of Minecraft from the official resources
//! - Launching the game in a vanilla fashion (No additional Java classes)
//! - [Authentication](https://docs.rs/cobble-core/*/cobble_core/profile/struct.CobbleProfile.html) with Microsoft/Minecraft servers for online play
//! - Usage of [instances](https://docs.rs/cobble-core/*/cobble_core/instance/struct.Instance.html) to ease [installation](https://docs.rs/cobble-core/*/cobble_core/instance/struct.Instance.html#method.full_installation), [launching](https://docs.rs/cobble-core/*/cobble_core/instance/struct.Instance.html#method.launch) and managing multiple installs
//! - Supports installing and launching with the fabric loader.
//! - Managing of various objects used and created by Minecraft (Logs, Resourcepacks, Save Games, Screenshots, Servers, Mods, Shaderpacks)
//!
//! ## Usage
//!
//! Add this to your `Cargo.toml`:
//!
//! ```toml
//! [dependencies]
//! cobble-core = "1.2"
//! ```
//!
//! To get started, see various examples of this crate [here](https://gitlab.com/stefan99353/cobble-core/-/tree/main/examples).
//!
//! This crate is based on the tokio async crate. Some functionality requires a tokio runtime.
//!
//! ## Stability
//!
//! This crate can't be tested using every Minecraft version there is (I sadly do not have time for that).
//! This means I try to test this crate with some different versions.
//! Most tests are for the newer releases of Minecraft.
//!
//! The following versions have been tested:
//!
//! - 1.19.2
//!
//! *If you have success using different Minecraft versions, you can open a Pull Request to add it.*
//!
//! ## Crate Features
//!
//! - `auth`: Provides authentication support for online mode.
//! - `backup`: Provides functionality creating and loading backups. Currently implemented for `save-games`.
//! - `serde`: Provides `Deserialize` and `Serialize` implementation for many structs.
//! - `vanilla` (default): Includes features `log-files`, `resourcepacks`, `save-games`, `screenshots` and `servers`.
//! - `log-files` (default): Provides functionality for reading and extracting log files.
//! - `resourcepacks` (default): Provides functionality for interacting with resourcepacks.
//! - `save-games` (default): Provides functionality for interacting with save games.
//! - `screenshots` (default): Provides functionality for interacting with screenshots.
//! - `servers` (default): Provides functionality for interacting with servers.
//! - `modded`: Includes features `fabric`, `loader-mods` and `shaderpacks`.
//! - `fabric`: Provides functionality for installing and launching with the fabric loader.
//! - `loader-mods`: Provides functionality for interacting with mods.
//! - `shaderpacks`: Provides functionality for interacting with shaderpacks.
//!
//! ## License
//!
//! `cobble-core` is distributed under the terms of the MIT license.
//!
//! See [LICENSE](https://gitlab.com/stefan99353/cobble-core/-/tree/main/LICENSE) for details.

#![warn(missing_docs)]
#![warn(
    missing_debug_implementations,
    clippy::print_stderr,
    clippy::print_stdout
)]
#![cfg_attr(doc_cfg, feature(doc_cfg))]

#[macro_use]
extern crate tracing;

pub(crate) mod consts;
/// Error types
pub mod error;
/// Instance related functionality
pub mod instance;
/// Minecraft related functionality
pub mod minecraft;
/// Utitlities for authentication and online play
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub mod profile;
/// Utilities used in this crate
pub mod utils;

pub use instance::{Instance, InstanceBuilder};
