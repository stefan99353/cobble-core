use crate::error::{InstallationError, LaunchError};

/// Result with [`CobbleError`](CobbleError) as the error type.
pub type CobbleResult<T> = Result<T, CobbleError>;

/// A general error that can occur when working with this crate.
#[derive(Debug, thiserror::Error)]
pub enum CobbleError {
    /// Minecraft version of the instance does not exist.
    #[error("Version '{0}' is not a valid minecraft version")]
    InvalidVersion(String),
    /// Instance will not be launched, because it is not marked as installed.
    #[error("Instance is not marked as fully installed")]
    NotInstalled,
    /// Error while installing.
    #[error("{0}")]
    Installation(InstallationError),
    /// Error while launching.
    #[error("{0}")]
    Launch(LaunchError),
    /// IO error.
    #[error("{0}")]
    Io(std::io::Error),
    /// Serialization/Deserialization error while working with JSON.
    #[error("Deserialization/Serialization: {0}")]
    Serde(serde_json::Error),
    /// Error while downloading.
    #[error("{0}")]
    DownloadMetadata(reqwest::Error),
    /// Error while waiting for blocking code to finish.
    #[error("{0}")]
    TokioJoinError(tokio::task::JoinError),

    /// Error while working with archives.
    #[cfg_attr(
        doc_cfg,
        doc(cfg(any(
            feature = "log-files",
            feature = "resourcepacks",
            feature = "loader-mods"
        )))
    )]
    #[cfg(any(
        feature = "log-files",
        feature = "resourcepacks",
        feature = "loader-mods"
    ))]
    #[error("{0}")]
    Zip(async_zip::error::ZipError),

    /// Error while deserializing/serializing an NBT file
    #[cfg_attr(doc_cfg, doc(cfg(feature = "internal-nbt")))]
    #[cfg(feature = "internal-nbt")]
    #[error("{0}")]
    NbtSerde(fastnbt::error::Error),

    /// Error while decoding a base64 string.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "internal-base64")))]
    #[cfg(feature = "internal-base64")]
    #[error("{0}")]
    Base64Decode(base64::DecodeError),

    /// Import archive is missing a marker file
    #[cfg_attr(doc_cfg, doc(cfg(any(feature = "save-games"))))]
    #[cfg(any(feature = "save-games"))]
    #[error("Import archive is missing a marker file")]
    MissingMarkerFile,
}

impl From<InstallationError> for CobbleError {
    fn from(err: InstallationError) -> Self {
        Self::Installation(err)
    }
}

impl From<LaunchError> for CobbleError {
    fn from(err: LaunchError) -> Self {
        Self::Launch(err)
    }
}

impl From<std::io::Error> for CobbleError {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<serde_json::Error> for CobbleError {
    fn from(err: serde_json::Error) -> Self {
        Self::Serde(err)
    }
}

impl From<reqwest::Error> for CobbleError {
    fn from(err: reqwest::Error) -> Self {
        Self::DownloadMetadata(err)
    }
}

impl From<tokio::task::JoinError> for CobbleError {
    fn from(err: tokio::task::JoinError) -> Self {
        Self::TokioJoinError(err)
    }
}

#[cfg(any(
    feature = "log-files",
    feature = "resourcepacks",
    feature = "loader-mods"
))]
impl From<async_zip::error::ZipError> for CobbleError {
    fn from(err: async_zip::error::ZipError) -> Self {
        Self::Zip(err)
    }
}

#[cfg(feature = "internal-nbt")]
impl From<fastnbt::error::Error> for CobbleError {
    fn from(err: fastnbt::error::Error) -> Self {
        Self::NbtSerde(err)
    }
}

#[cfg(feature = "internal-base64")]
impl From<base64::DecodeError> for CobbleError {
    fn from(err: base64::DecodeError) -> Self {
        Self::Base64Decode(err)
    }
}
