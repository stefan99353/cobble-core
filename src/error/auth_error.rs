/// Result with [`AuthError`](AuthError) as the error type.
pub type AuthResult<T> = Result<T, AuthError>;

/// An error that occurs during authentication.
#[derive(Debug, thiserror::Error)]
pub enum AuthError {
    /// Failed to parse an URL
    #[error("Failed to parse an URL")]
    ParseUrl(oauth2::url::ParseError),
    /// There was a problem configuring the request
    #[error("There was a problem configuring the request: {0}")]
    ConfigurationError(oauth2::ConfigurationError),
    /// Error while requesting access token
    #[error("Error while requesting access token: {0}")]
    RequestTokenError(String),
    /// Microsoft did not provide a refresh token
    #[error("Microsoft did not provide a refresh token")]
    NoRefreshToken,
    /// Error while performing a web request
    #[error("{0}")]
    Request(reqwest::Error),
    /// XBoxLive auth did not return a user hash
    #[error("XBoxLive auth did not return a user hash")]
    NoXblUserHash,
    /// User is missing entitlements
    #[error("User is missing entitlements")]
    Unauthorized,
}

impl From<oauth2::url::ParseError> for AuthError {
    fn from(err: oauth2::url::ParseError) -> Self {
        Self::ParseUrl(err)
    }
}

impl From<oauth2::ConfigurationError> for AuthError {
    fn from(err: oauth2::ConfigurationError) -> Self {
        Self::ConfigurationError(err)
    }
}

impl From<reqwest::Error> for AuthError {
    fn from(err: reqwest::Error) -> Self {
        Self::Request(err)
    }
}
