use crate::error::DownloadError;

/// Result with [`InstallationError`](InstallationError) as the error type.
pub type InstallationResult<T> = Result<T, InstallationError>;

/// An error that occurs during installation of minecraft.
#[derive(Debug, thiserror::Error)]
pub enum InstallationError {
    /// Error while downloading a resource.
    #[error("Download failure: {0}")]
    Download(DownloadError),
    /// Error while interacting with IO.
    #[error("IO error while installing minecraft: {0}")]
    Io(std::io::Error),
    /// Error while extracting a library.
    #[error("Library extration failure: {0}")]
    Extract(async_zip::error::ZipError),
    /// Parsing the SHA1 checksum failed.
    #[error("Parsing the SHA1 checksum failed: {0}")]
    ParseChecksum(hex::FromHexError),
    /// Serializing of the asset index failed
    #[error("Serializing of the version data or asset index failed: {0}")]
    JsonSerialize(serde_json::Error),
}

impl From<DownloadError> for InstallationError {
    fn from(err: DownloadError) -> Self {
        Self::Download(err)
    }
}

impl From<std::io::Error> for InstallationError {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<async_zip::error::ZipError> for InstallationError {
    fn from(err: async_zip::error::ZipError) -> Self {
        Self::Extract(err)
    }
}

impl From<hex::FromHexError> for InstallationError {
    fn from(err: hex::FromHexError) -> Self {
        Self::ParseChecksum(err)
    }
}

impl From<serde_json::Error> for InstallationError {
    fn from(err: serde_json::Error) -> Self {
        Self::JsonSerialize(err)
    }
}
