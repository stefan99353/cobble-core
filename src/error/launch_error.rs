/// Result with [`LaunchError`](LaunchError) as the error type.
pub type LaunchResult<T> = Result<T, LaunchError>;

/// An error that occurs during installation of minecraft.
#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
pub enum LaunchError {
    /// IO error
    #[error("{0}")]
    Io(std::io::Error),
    /// Failed to send stop signal to game process
    #[cfg_attr(doc_cfg, doc(cfg(unix)))]
    #[cfg(unix)]
    #[error("{0}")]
    Ipc(ipc_channel::ipc::IpcError),
    /// Failed to fork game process
    #[cfg_attr(doc_cfg, doc(cfg(unix)))]
    #[cfg(unix)]
    #[error("Failed to fork game process")]
    ProcessForking,
    /// Failed to wait for the game process to exit.
    /// When this error occurs, there may be a zombie process left.
    #[cfg_attr(doc_cfg, doc(cfg(unix)))]
    #[cfg(unix)]
    #[error("Failed to wait for the game process to exit. There may be a zombie process left.")]
    WaitPid,
}

impl From<std::io::Error> for LaunchError {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}

#[cfg(unix)]
impl From<ipc_channel::ipc::IpcError> for LaunchError {
    fn from(err: ipc_channel::ipc::IpcError) -> Self {
        Self::Ipc(err)
    }
}
