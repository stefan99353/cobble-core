/// URL of the Minecraft version manifest.
pub const MC_VERSION_MANIFEST_URL: &str =
    "https://launchermeta.mojang.com/mc/game/version_manifest.json";
/// Base URL for Minecraft libraries.
pub const MC_LIBRARIES_BASE_URL: &str = "https://libraries.minecraft.net";
/// Base URL for Minecraft assets.
pub const MC_ASSETS_BASE_URL: &str = "https://resources.download.minecraft.net";

/// Base URL for the fabric v2 API.
#[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
#[cfg(feature = "fabric")]
pub const FABRIC_BASE_V2_URL: &str = "https://meta.fabricmc.net/v2";

/// Microsoft auth URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const MS_AUTH_URL: &str = "https://login.microsoftonline.com/consumers/oauth2/v2.0/authorize";
/// Microsoft token URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const MS_TOKEN_URL: &str = "https://login.microsoftonline.com/consumers/oauth2/v2.0/token";
/// Microsoft device auth URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const MS_DEVICE_AUTH_URL: &str =
    "https://login.microsoftonline.com/consumers/oauth2/v2.0/devicecode";
/// XBoxLive token URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const XBL_TOKEN_URL: &str = "https://user.auth.xboxlive.com/user/authenticate";
/// XBoxLive Security token URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const XSTS_TOKEN_URL: &str = "https://xsts.auth.xboxlive.com/xsts/authorize";
/// Minecraft token URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const MC_TOKEN_URL: &str = "https://api.minecraftservices.com/authentication/login_with_xbox";
/// Minecraft entitlements URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const MC_ENTITLEMENTS_URL: &str = "https://api.minecraftservices.com/entitlements/mcstore";
/// Minecraft profile URL
#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub const MC_PROFILE_URL: &str = "https://api.minecraftservices.com/minecraft/profile";
