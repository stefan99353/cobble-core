use crate::minecraft::models::file::File;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Download information of a library.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LibraryDownloads {
    /// Library file information.
    pub artifact: Option<File>,
    /// Library classifiers for natives.
    #[serde(default)]
    pub classifiers: HashMap<String, File>,
}
