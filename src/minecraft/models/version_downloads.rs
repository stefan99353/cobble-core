use crate::minecraft::models::File;
use serde::{Deserialize, Serialize};

/// Download information of the client/server.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct VersionDownloads {
    /// Client file
    pub client: File,
    /// Client mappings
    pub client_mappings: Option<File>,
    /// Server file
    pub server: Option<File>,
    /// Server mappings
    pub server_mappings: Option<File>,
}
