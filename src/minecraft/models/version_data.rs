use crate::minecraft::models::{
    AssetIndexInfo, LaunchArguments, Library, LoggingInfo, RecommendedJavaVersion,
    VersionDownloads, VersionType,
};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

/// Version data includes all necessary information for installing and launching Minecraft.
/// Every Minecraft version (releases, snapshots, betas, ...) has its own version data.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct VersionData {
    /// Game and JVM arguments
    pub arguments: Option<LaunchArguments>,
    /// Information of the asset index
    #[serde(alias = "assetIndex")]
    pub asset_index: AssetIndexInfo,
    /// ID of the used asset index
    pub assets: String,
    /// Compliance level
    #[serde(alias = "complianceLevel")]
    pub compliance_level: i32,
    /// Download information of the client/server
    pub downloads: Option<VersionDownloads>,
    /// Version ID
    pub id: String,
    /// Recommended java version
    #[serde(alias = "javaVersion")]
    pub java_version: RecommendedJavaVersion,
    /// Libraries
    pub libraries: Vec<Library>,
    /// Logging information
    pub logging: Option<LoggingInfo>,
    /// Main class / entry point
    #[serde(alias = "mainClass")]
    pub main_class: String,
    /// Minecraft arguments of older versions
    #[serde(alias = "minecraftArguments")]
    pub minecraft_arguments: Option<String>,
    /// Minimum version of the original launcher
    #[serde(alias = "minimumLauncherVersion")]
    pub minimum_launcher_version: i32,
    /// Release time
    #[serde(alias = "releaseTime", with = "time::serde::rfc3339")]
    pub release_time: OffsetDateTime,
    /// Release time
    #[serde(with = "time::serde::rfc3339")]
    pub time: OffsetDateTime,
    /// Type of the version
    #[serde(rename = "type")]
    pub _type: VersionType,
}

impl VersionData {
    /// Gets the version data from minecraft server.
    pub async fn fetch(url: &str) -> reqwest::Result<Self> {
        reqwest::get(url).await?.error_for_status()?.json().await
    }

    /// Returns all needed libraries by applying the rules of the libraries.
    pub fn needed_libraries(&self) -> Vec<&Library> {
        self.libraries
            .iter()
            .filter(|library| library.check_use())
            .collect()
    }
}
