use crate::minecraft::models::AssetIndex;
use serde::{Deserialize, Serialize};

/// Information of the asset index.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AssetIndexInfo {
    /// The ID of the index
    pub id: String,
    /// SHA1 of the index JSON
    pub sha1: String,
    /// Size of the index JSON
    pub size: usize,
    /// Size of all the assets contained in the index JSON
    #[serde(alias = "totalSize")]
    pub total_size: i64,
    /// Url of the index JSON
    pub url: String,
}

impl AssetIndexInfo {
    /// Gets the index itself from Minecraft servers.
    pub async fn fetch_index(&self) -> reqwest::Result<AssetIndex> {
        reqwest::get(&self.url)
            .await?
            .error_for_status()?
            .json()
            .await
    }
}
