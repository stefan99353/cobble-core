use crate::error::CobbleResult;
use async_zip::read::seek::ZipFileReader;
use std::path::PathBuf;
use tokio::fs::{remove_file, File};

/// Represents a single resourcepack.
#[cfg_attr(doc_cfg, doc(cfg(feature = "resourcepacks")))]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Debug)]
pub struct Resourcepack {
    /// Filename
    pub name: String,
    /// Filepath
    pub path: PathBuf,
    /// Type
    pub _type: ResourcepackType,
}

impl Resourcepack {
    /// Removes the resourcepack from disk.
    ///
    /// **Warning**: This will permanently delete the file!
    #[instrument(
        name = "remove_resourcepack",
        level = "trace",
        skip_all,
        fields(
            name,
            path = %self.path.to_string_lossy(),
        )
    )]
    pub async fn remove(self) -> CobbleResult<()> {
        remove_file(&self.path).await?;
        Ok(())
    }

    /// Loads the icon from the resourcepack archive.
    #[instrument(
        name = "resourcepack_icon",
        level = "trace",
        skip_all,
        fields(
            name = self.name,
            path = %self.path.to_string_lossy(),
        )
    )]
    pub async fn icon(&self) -> CobbleResult<Option<Vec<u8>>> {
        trace!("Trying to extract pack.png from archive");
        let mut file = File::open(&self.path).await?;
        let mut archive = ZipFileReader::new(&mut file).await?;

        let icon = match archive.entry("pack.png") {
            Some((i, _entry)) => {
                trace!("Found pack.png file");
                let entry_reader = archive.entry_reader(i).await?;
                let bytes = entry_reader.read_to_end_crc().await?;
                Some(bytes)
            }
            None => {
                trace!("Resourcepack does not contain a pack.png file");
                None
            }
        };

        Ok(icon)
    }
}

/// Whether the file is a resourcepack or texturepack.
#[cfg_attr(doc_cfg, doc(cfg(feature = "resourcepacks")))]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Copy, Debug)]
pub enum ResourcepackType {
    /// Resourcepack (>= Minecraft 1.6.1)
    Resourcepack,
    /// Texturepack (< Minecraft 1.6.1)
    Texturepack,
}

impl Resourcepack {
    /// All minecraft versions that were using texturepacks instead of resourcepacks.
    /// Versions before 2013-06-13.
    pub const TEXTUREPACK_VERSIONS: [&str; 94] = [
        "rd-132211",
        "rd-132328",
        "rd-20090515",
        "rd-160052",
        "rd-161348",
        "c0.0.11a",
        "c0.0.13a_03",
        "c0.0.13a",
        "c0.30_01c",
        "inf-20100618",
        "a1.0.4",
        "a1.0.5_01",
        "a1.0.11",
        "a1.0.14",
        "a1.0.15",
        "a1.0.16",
        "a1.0.17_02",
        "a1.0.17_04",
        "a1.1.0",
        "a1.1.2",
        "a1.1.2_01",
        "a1.2.0",
        "a1.2.0_01",
        "a1.2.0_02",
        "a1.2.1",
        "a1.2.1_01",
        "a1.2.2a",
        "a1.2.2b",
        "a1.2.3",
        "a1.2.3_01",
        "a1.2.3_02",
        "a1.2.3_04",
        "a1.2.4_01",
        "a1.2.5",
        "a1.2.6",
        "b1.0",
        "b1.0_01",
        "b1.0.2",
        "b1.1_01",
        "b1.1_02",
        "b1.2",
        "b1.2_01",
        "b1.2_02",
        "b1.3b",
        "b1.3_01",
        "b1.4",
        "b1.4_01",
        "b1.5",
        "b1.5_01",
        "b1.6",
        "b1.6.1",
        "b1.6.2",
        "b1.6.3",
        "b1.6.4",
        "b1.6.5",
        "b1.6.6",
        "b1.7",
        "b1.7.2",
        "b1.7.3",
        "b1.8",
        "b1.8.1",
        "1.0",
        "1.1",
        "1.2.1",
        "1.2.2",
        "1.2.3",
        "1.2.4",
        "1.2.5",
        "1.3",
        "1.3.1",
        "1.3.2",
        "1.4",
        "1.4.1",
        "1.4.2",
        "1.4.3",
        "1.4.4",
        "1.4.6",
        "1.4.5",
        "1.4.7",
        "1.5",
        "1.5.1",
        "13w16a",
        "13w16b",
        "1.5.2",
        "13w17a",
        "13w18a",
        "13w18b",
        "13w18c",
        "13w19a",
        "13w21a",
        "13w21b",
        "13w22a",
        "13w23a",
        "13w23b",
    ];
}
