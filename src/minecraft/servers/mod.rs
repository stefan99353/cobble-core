mod server;

use crate::error::CobbleResult;
pub use server::*;
use std::path::{Path, PathBuf};
use tokio::task;

/// Loads all servers from the minecraft folder.
#[cfg_attr(doc_cfg, doc(cfg(feature = "servers")))]
#[instrument(name = "load_servers", level = "debug", skip_all, fields(servers_file))]
pub async fn load_servers(servers_file: impl AsRef<Path> + Send) -> CobbleResult<Vec<Server>> {
    if !servers_file.as_ref().is_file() {
        trace!("servers.dat does not exist.");
        return Ok(vec![]);
    }

    trace!("Reading servers from file...");
    let servers_file = PathBuf::from(servers_file.as_ref());
    task::spawn_blocking(move || {
        let nbt_file = std::fs::File::open(&servers_file)?;

        let servers = fastnbt::from_reader::<_, ServersDat>(nbt_file)?
            .servers
            .into_iter()
            .map(|server| {
                let accept_textures = match server.accept_textures {
                    Some(0) => AcceptTextures::Disabled,
                    Some(1) => AcceptTextures::Enabled,
                    _ => AcceptTextures::Prompt,
                };

                Server {
                    name: server.name,
                    ip: server.ip,
                    accept_textures,
                    path: servers_file.clone(),
                }
            })
            .collect::<Vec<_>>();

        Ok(servers)
    })
    .await?
}
