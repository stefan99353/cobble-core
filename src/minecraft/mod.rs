mod install;
mod launch;
/// Models for interacting with Minecraft servers
pub mod models;
mod utils;

pub use install::*;
pub use launch::*;

// Vanilla features
#[cfg(feature = "log-files")]
mod log_files;
#[cfg(feature = "resourcepacks")]
mod resourcepacks;
#[cfg(feature = "save-games")]
mod save_games;
#[cfg(feature = "screenshots")]
mod screenshots;
#[cfg(feature = "servers")]
mod servers;

#[cfg(feature = "log-files")]
pub use log_files::*;
#[cfg(feature = "resourcepacks")]
pub use resourcepacks::*;
#[cfg(feature = "save-games")]
pub use save_games::*;
#[cfg(feature = "screenshots")]
pub use screenshots::*;
#[cfg(feature = "servers")]
pub use servers::*;

// Modded features
#[cfg(feature = "fabric")]
mod fabric;
#[cfg(feature = "loader-mods")]
mod loader_mods;
#[cfg(feature = "shaderpacks")]
mod shaderpacks;

#[cfg(feature = "fabric")]
pub use fabric::*;
#[cfg(feature = "loader-mods")]
pub use loader_mods::*;
#[cfg(feature = "shaderpacks")]
pub use shaderpacks::*;
