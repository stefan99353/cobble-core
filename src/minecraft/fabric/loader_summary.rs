use crate::consts;
use serde::{Deserialize, Serialize};

/// An entry in the fabric loader manifest representing a single version.
#[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FabricLoaderSummary {
    /// Information about the loader.
    pub loader: FabricLoaderInfo,
    /// Information about the intermediary.
    pub intermediary: FabricIntermediaryInfo,
}

impl FabricLoaderSummary {
    /// Gets the manifest from fabric servers.
    pub async fn fetch_manifest(game_version: &str) -> reqwest::Result<Vec<Self>> {
        let url = Self::version_url(game_version);

        reqwest::get(url)
            .await?
            .error_for_status()?
            .json::<Vec<Self>>()
            .await
    }

    fn version_url(game_version: &str) -> String {
        format!(
            "{}/versions/loader/{}",
            consts::FABRIC_BASE_V2_URL,
            game_version
        )
    }
}

/// Information about the loader.
#[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FabricLoaderInfo {
    /// Separator
    pub separator: String,
    /// Build
    pub build: i32,
    /// Maven
    pub maven: String,
    /// Version
    pub version: String,
    /// Stable
    pub stable: bool,
}

/// Information about the intermediary.
#[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FabricIntermediaryInfo {
    /// Maven
    pub maven: String,
    /// Version
    pub version: String,
    /// Stable
    pub stable: bool,
}
