use crate::minecraft::models::VersionData;
use std::path::{Path, PathBuf};

pub fn build_logging_arg(
    version_data: &VersionData,
    log_configs_path: impl AsRef<Path>,
) -> Option<String> {
    version_data.logging.as_ref().map(|logging_info| {
        let mut path = PathBuf::from(log_configs_path.as_ref());

        path.push(
            logging_info
                .client
                .file
                .id
                .as_ref()
                .expect("Logging info has no ID"),
        );

        logging_info
            .client
            .argument
            .clone()
            .replace("${path}", &path.to_string_lossy())
    })
}
