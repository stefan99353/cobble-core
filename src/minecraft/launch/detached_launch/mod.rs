#[cfg(unix)]
mod unix;
#[cfg(windows)]
mod windows;

#[cfg(unix)]
pub use unix::GameProcessHandle;

#[cfg(windows)]
pub use windows::GameProcessHandle;

use crate::error::LaunchResult;
use async_trait::async_trait;
use std::process::Command;

/// Trait for game handles to provide the same API on all platforms.
#[async_trait]
pub trait GameProcess {
    /// Should launch the game in a detached state.
    fn launch(command: Command) -> LaunchResult<Self>
    where
        Self: Sized;
    /// Should stop the game.
    async fn stop(&self) -> LaunchResult<()>;
    /// Should wait until the game has exited.
    async fn wait(&self) -> LaunchResult<()>;
    /// Should check whether the game is still running.
    async fn is_stopped(&self) -> LaunchResult<bool>;
    /// Should check whether the game is still running.
    fn is_stopped_blocking(&self) -> LaunchResult<bool>;
}
