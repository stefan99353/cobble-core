use crate::minecraft::models::VersionData;
use crate::minecraft::LaunchOptions;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct ArgumentReplacements {
    replacements: HashMap<String, String>,
}

impl ArgumentReplacements {
    pub fn build(
        options: &LaunchOptions,
        version_data: &VersionData,
        classpath: String,
        minecraft_path: String,
        assets_path: String,
        natives_path: String,
    ) -> Self {
        let mut map = HashMap::new();

        map.insert(
            "${auth_player_name}".to_string(),
            options.player_name.clone(),
        );
        map.insert("${user_properties}".to_string(), "{}".to_string());
        map.insert("${game_directory}".to_string(), minecraft_path.clone());
        map.insert(
            "${assets_index_name}".to_string(),
            version_data.assets.clone(),
        );
        map.insert("${assets_root}".to_string(), assets_path);
        map.insert(
            "${game_assets}".to_string(),
            format!("{}/assets", minecraft_path),
        );
        map.insert("${natives_directory}".to_string(), natives_path);
        map.insert("${version_name}".to_string(), version_data.id.clone());
        map.insert(
            "${version_type}".to_string(),
            version_data._type.to_string(),
        );
        map.insert(
            "${launcher_name}".to_string(),
            options.launcher_name.clone(),
        );
        map.insert(
            "${launcher_version}".to_string(),
            options.launcher_version.clone(),
        );
        map.insert("${classpath}".to_string(), classpath);

        match (&options.profile_id, &options.token) {
            (Some(profile_id), Some(token)) => {
                map.insert("${auth_uuid}".to_string(), profile_id.clone());
                map.insert("${auth_access_token}".to_string(), token.clone());
                map.insert("${auth_session}".to_string(), token.clone());
                map.insert("${user_type}".to_string(), "msa".to_string());
            }
            _ => {
                map.insert("${auth_uuid}".to_string(), String::from("X"));
                map.insert("${auth_access_token}".to_string(), String::from("X"));
                map.insert("${auth_session}".to_string(), String::from("X"));
                map.insert("${user_type}".to_string(), String::from("msa"));
            }
        }

        Self { replacements: map }
    }

    pub fn replace(&self, mut arg: String) -> String {
        for (pattern, value) in &self.replacements {
            arg = arg.replace(pattern, value);
        }

        arg
    }
}
