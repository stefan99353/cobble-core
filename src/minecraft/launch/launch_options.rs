use derive_builder::Builder;

/// Options for launching Minecraft.
/// The options can be created using the builder pattern:
///
/// ```rust
/// # use cobble_core::minecraft::{LaunchOptions, LaunchOptionsBuilder};
/// # fn function() {
///
/// let options: LaunchOptions = LaunchOptionsBuilder::default()
///     .player_name("Steve".to_string())
///     .fullscreen(true)
///     .java_executable("/usr/bin/java".to_string())
///     .build();
///
/// # }
/// ```
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Builder, Clone, Debug)]
#[builder(build_fn(private, name = "fallible_build"))]
pub struct LaunchOptions {
    /// Name of the launcher that gets passed as a game argument.
    ///
    /// Defaults to the crates name.
    #[builder(setter(into), default = "String::from(env!(\"CARGO_PKG_NAME\"))")]
    pub(crate) launcher_name: String,

    /// Version of the launcher that gets passed as a game argument.
    ///
    /// Defaults to the crates version.
    #[builder(setter(into), default = "String::from(env!(\"CARGO_PKG_VERSION\"))")]
    pub(crate) launcher_version: String,

    /// Player name for the game.
    ///
    /// Defaults to `Steve`.
    #[builder(setter(into), default = "String::from(\"Steve\")")]
    pub(crate) player_name: String,

    /// The profile ID.
    /// This is needed for online mode.
    ///
    /// Default to `None`.
    #[builder(setter(strip_option), default)]
    pub(crate) profile_id: Option<String>,

    /// The minecraft access token.
    /// This is needed for online mode.
    ///
    /// Defaults to `None`.
    #[builder(setter(strip_option), default)]
    pub(crate) token: Option<String>,

    /// Enables fullscreen.
    /// Overwrites the custom window size.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub(crate) fullscreen: bool,

    /// Enables custom window size.
    /// Takes effect when not launching in fullscreen.
    /// The width and height can be configured with `custom_width` and `custom_height`.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub(crate) enable_custom_window_size: bool,

    /// Custom game window width.
    /// Used when `enable_custom_window_size` is enabled.
    ///
    /// Defaults to `1280`.
    #[builder(default = "1280")]
    pub(crate) custom_width: u32,

    /// Custom game window height.
    /// Used when `enable_custom_window_size` is enabled.
    ///
    /// Defaults to `720`.
    #[builder(default = "720")]
    pub(crate) custom_height: u32,

    /// Enables custom JVM memory restrictions.
    /// The minimum and maximum can be configured with `custom_min_memory` and `custom_max_memory`.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub(crate) enable_custom_memory: bool,

    /// JVM initial heap size in megabytes.
    /// Adds the `-Xms` option to the command.
    /// Gets added before `custom_jvm_args`.
    /// Used when `enable_custom_memory` is enabled.
    ///
    /// Defaults to `1024`.
    #[builder(default = "1024")]
    pub(crate) custom_min_memory: u32,

    /// JVM max heap size in megabytes.
    /// Adds the `-Xmx` opti`on to the command.
    /// Gets added before `custom_jvm_args`.
    /// Used when `enable_custom_memory` is enabled.
    ///
    /// Defaults to `2048`.
    #[builder(default = "2048")]
    pub(crate) custom_max_memory: u32,

    /// Java executable.
    ///
    /// Defaults to `java`.
    #[builder(setter(into), default = "String::from(\"java\")")]
    pub(crate) java_executable: String,

    /// Custom JVM arguments
    ///
    /// Defaults to `None`.
    #[builder(setter(into, strip_option), default)]
    pub(crate) custom_jvm_arguments: Option<String>,

    /// Environment variables used for launching the game.
    ///
    /// Defaults to `vec![]`.
    #[builder(default)]
    pub(crate) environment_variables: Vec<(String, Option<String>)>,
}

impl Default for LaunchOptions {
    fn default() -> Self {
        LaunchOptionsBuilder::default().build()
    }
}

impl LaunchOptionsBuilder {
    /// Builds new `LaunchOptions`.
    pub fn build(&self) -> LaunchOptions {
        self.fallible_build().unwrap()
    }
}
