use crate::minecraft::models::VersionData;
use std::path::{Path, PathBuf};

pub fn client_jar_path(version_data: &VersionData, minecraft_path: impl AsRef<Path>) -> PathBuf {
    let mut minecraft_path = PathBuf::from(minecraft_path.as_ref());
    minecraft_path.push("bin");
    minecraft_path.push(format!("minecraft-{}-client.jar", &version_data.id));

    minecraft_path
}
