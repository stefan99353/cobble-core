use cobble_core::minecraft::models::{VersionData, VersionManifest};
use cobble_core::minecraft::{build_launch_command, GameProcess, GameProcessHandle, LaunchOptions};
use std::env::temp_dir;
use tracing_subscriber::FmtSubscriber;

#[tokio::main]
async fn main() {
    FmtSubscriber::builder()
        .with_env_filter("info,cobble_core=debug")
        .init();

    // Fetch version manifest
    let version_manifest = VersionManifest::fetch().await.unwrap();

    // Get version summary
    let version_summary = version_manifest
        .versions
        .get(&version_manifest.latest.release)
        .unwrap();

    // Fetch version data
    let version_data = VersionData::fetch(&version_summary.url).await.unwrap();

    // Launch options
    let options = LaunchOptions::default();

    // Paths
    let mut libraries_path = temp_dir();
    libraries_path.push("cobble-core/libraries");
    let mut natives_path = temp_dir();
    natives_path.push("cobble-core/natives");
    let mut assets_path = temp_dir();
    assets_path.push("cobble-core/assets");
    let mut log_configs_path = temp_dir();
    log_configs_path.push("cobble-core/assets/log_configs");
    let mut minecraft_path = temp_dir();
    minecraft_path.push("cobble-core/.minecraft");

    let command = build_launch_command(
        &version_data,
        &options,
        minecraft_path,
        libraries_path,
        assets_path,
        natives_path,
        log_configs_path,
    );

    tracing::debug!("Command: {:?}", command);

    tracing::info!("Launching...");
    let handle = GameProcessHandle::launch(command).unwrap();
    tracing::info!("Waiting...");
    handle.wait().await.unwrap();
}
