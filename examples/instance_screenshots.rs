use cobble_core::Instance;
use tracing_subscriber::FmtSubscriber;

pub const INSTANCE_JSON: &str = include_str!("instance.json");

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    FmtSubscriber::builder()
        .with_env_filter("info,cobble_core=trace")
        .init();

    // Parse JSON
    let mut instance = serde_json::from_str::<Instance>(INSTANCE_JSON)?;
    instance.installed = true;

    let screenshots = instance.load_screenshots().await?;
    tracing::info!("{:#?}", screenshots);

    Ok(())
}
