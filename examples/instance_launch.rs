use cobble_core::minecraft::LaunchOptionsBuilder;
use cobble_core::Instance;
use std::process::Stdio;
use tracing_subscriber::FmtSubscriber;

pub const INSTANCE_JSON: &str = include_str!("instance.json");

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    FmtSubscriber::builder()
        .with_env_filter("info,cobble_core=debug")
        .init();

    // Parse JSON
    let mut instance = serde_json::from_str::<Instance>(INSTANCE_JSON)?;
    instance.installed = true;

    // Options
    let options = LaunchOptionsBuilder::default().build();

    let mut child = instance
        .launch(
            &options,
            Stdio::inherit(),
            Stdio::inherit(),
            Stdio::inherit(),
        )
        .await?;
    child.wait().await?;

    Ok(())
}
